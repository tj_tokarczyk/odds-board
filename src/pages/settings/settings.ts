import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  sportNameSetting = [];
  sportTimeSetting = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidLoad() {
    this.storage.get('sportNames').then((val) => {
      this.sportNames = val;
    });
    this.storage.get('sportNameSetting').then((val) => {
      this.sportNameSetting = JSON.parse(val);
    });

    this.storage.get('sportTimeSetting').then((val) => {
      this.sportTimeSetting = val;
    });

  }

  update(n, e){
    this.sportNameSetting[n] = e;
    this.storage.get('sportNameSetting').then(valueStr => {
        let value = JSON.parse(valueStr);
         value[n] = e;
         this.storage.set('sportNameSetting', JSON.stringify(value));
    });
  }

}
