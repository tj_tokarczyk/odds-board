import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ObjectToArrayPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'objectToArray',
})
export class ObjectToArrayPipe implements PipeTransform {
    transform(value, args:string[]) : any {
      let keys = [];
      for (let key in value) {
        keys.push(key);
      }
      return keys;
    }
}
