import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addPlusSign',
})
export class AddPlusSignPipe implements PipeTransform {
  transform(value: string) {
    if (parseFloat(value) > 0) {
        value = "+"+value;
        return value;
    } else {
      return value;
    }

  }
}
