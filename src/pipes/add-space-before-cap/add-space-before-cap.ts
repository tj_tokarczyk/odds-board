import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the AddSpaceBeforeCapPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'addSpaceBeforeCap',
})
export class AddSpaceBeforeCapPipe implements PipeTransform {
  transform(value: string) {
    return value.replace(/([A-Z])/g, ' $1').trim();
  }
}
