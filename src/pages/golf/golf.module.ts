import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GolfPage } from './golf';

@NgModule({
  declarations: [
    GolfPage,
  ],
  imports: [
    IonicPageModule.forChild(GolfPage),
  ],
})
export class GolfPageModule {}
