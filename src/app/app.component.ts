import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import moment from 'moment';

import { HomePage } from '../pages/home/home';
import { SportsPage } from '../pages/sports/sports';


@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  rootPage:any = SportsPage;
  sportNames = {};
  sportNameSetting = {};

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage, public http: HttpClient) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      http.get('http://mtbhere.com/odds.php').subscribe(data => {
          this.storage.set('dailyOdds', data);
          this.storage.set('lastChecked', moment().format("YYYY-MM-DD")+"T06:00:00-04:00");
      });


      this.sportNames =
        {
          0: "MLB",
          1: "NBA",
          2: "NCAAB",
          3: "NCAAF",
          4: "NFL",
          5: "NHL",
          7: "Soccer",
          8: "WNBA",
          9: "Tennis",
          10: "Boxing",
          11: "MMA",
          14: "KHL",
          15: "AHL",
          16: "SHL",
          17: "SHL",
          18: "SHL",
          19: "NPB",
          20: "KBO",
          21: "Golf",
          24: "CFL"
        }

      this.sportNameSetting =
        {
          "MLB": true,
          "NBA": true,
          "NCAAB": true,
          "NCAAF": true,
          "NFL": true,
          "NHL": true,
          "Soccer": true,
          "WNBA": true,
          "Tennis": true,
          "Boxing": true,
          "MMA": true,
          "KHL": true,
          "AHL": true,
          "SHL": true,
          "NPB": true,
          "KBO": true,
          "Golf": true,
          "CFL": true
        }

      this.sportTimeSetting = 5000;

      storage.set('sportNames', this.sportNames);

      storage.get('sportNameSetting').then((val) => {
        if(val == null){
          this.storage.set('sportNameSetting', JSON.stringify(this.sportNameSetting));
        }
      });

      storage.get('sportTimeSetting').then((val) => {
        if(val == null){
          this.storage.set('sportTimeSetting', this.sportTimeSetting);
        }
      });




      setInterval( function() {
        storage.get('lastChecked').then((val) => {
          if( moment(moment().format()).isAfter(moment(val).add(1,'d'))) {
            http.get('http://mtbhere.com/odds.php').subscribe(data => {
                this.storage.set('dailyOdds', data);
                this.storage.set('lastChecked', moment().format("YYYY-MM-DD")+"T06:00:00-04:00");
            });
          }
        });
      }, 5000);
    });
  }


}
