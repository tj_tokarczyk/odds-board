import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';


import { SportsPage } from '../sports/sports';

/**
 * Generated class for the GolfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-golf',
  templateUrl: 'golf.html',
})
export class GolfPage {

  pageTitle = "";
  results = <any>[];
  header = <any>[];
  sportNameSetting = <any>[];
  sportNames = <any>[];
  sportNamesFiltered = <any>[];
  nextPage = "";
  cardCounter = 0;
  scrollingVarible = "zero-scroll";
  scrollingCounter = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private storage: Storage) {

  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this.storage.get('sportNameSetting').then((val) => {
      this.sportNameSetting = JSON.parse(val);
    });
    this.storage.get('sportNames').then((val) => {
      this.pageTitle = val[this.navParams.get("sport")];
      this.sportNames = val;
      for (var key in this.sportNames) {
        if( this.sportNameSetting[this.sportNames[key]] ){
          this.sportNamesFiltered[key] = this.sportNames[key];
        }
      }

    });
    this.storage.get('dailyOdds').then((val) => {
      var i;
      for (i = 0; i < val.length; i++) {
        this.header.push(val[i].Sport);
        this.header = this.header.filter( this.onlyUnique );
      }
      var i;
      for (i = 0; i < val.length; i++) {
        if( this.navParams.get("sport") == val[i].Sport){
          this.results.push(val[i]);

        }
      }
    });

    setTimeout(function(){
       this.animateContentDown();
    }.bind(this), 5000);

  }

  autoSwapPages(){
    var i;
    for (i = 0; i < this.header.length; i++) {
      if( this.header[i] == this.getKeyByValue(this.sportNames, this.pageTitle)){
        var nextPageIndex = i + 1;
        if( nextPageIndex == this.header.length ){
          this.nextPage = this.sportNames[this.header[0]];
        } else {
          this.nextPage = this.sportNames[this.header[nextPageIndex]];
        }
        console.log(this.nextPage);
        if( this.nextPage == "Golf"){
          this.navCtrl.push(GolfPage, {sport: "Golf" });
        } else {
          this.navCtrl.push(SportsPage, {sport: this.getKeyByValue(this.sportNames, this.nextPage)});
        }
      }
    }
  }

  animateContentDown(){
    this.cardCounter = this.results.length;
    var total = Math.round(this.cardCounter / 9);
    if( total > 1 ){
        if( total >= this.scrollingCounter ){
          this.scrollingVarible = "zero-scroll-" + this.scrollingCounter;
          this.scrollingCounter++;
          setTimeout(function(){
             this.animateContentDown();
          }.bind(this), 5000);
        } else {
          this.autoSwapPages();
        }
    } else {
      this.autoSwapPages();
    }

  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }


}
