import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule} from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SportsPage } from '../pages/sports/sports';
import { GolfPage } from '../pages/golf/golf';
import { SettingsPage } from '../pages/settings/settings';

import { AddSpaceBeforeCapPipe } from '../pipes/add-space-before-cap/add-space-before-cap';
import { ChangeToLocalTimePipe } from '../pipes/change-to-local-time/change-to-local-time';
import { AddPlusSignPipe } from '../pipes/add-plus-sign/add-plus-sign';
import { ObjectToArrayPipe } from '../pipes/object-to-array/object-to-array';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SportsPage,
    GolfPage,
    SettingsPage,
    AddSpaceBeforeCapPipe,
    ChangeToLocalTimePipe,
    ObjectToArrayPipe,
    AddPlusSignPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GolfPage,
    SportsPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
