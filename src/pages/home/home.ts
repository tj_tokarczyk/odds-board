import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import { SportsPage } from '../sports/sports';
import { GolfPage } from '../golf/golf';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  results = <any>[];
  sportNames = <any>[];
  sportNameSetting = <any>[];
  sportNamesFiltered = <any>[];

  constructor(public navCtrl: NavController, public http: HttpClient, private storage: Storage) {

  }

  ionViewDidLoad() {

  }

  ionViewWillEnter(){
    this.storage.get('sportNameSetting').then((val) => {
      this.sportNameSetting = JSON.parse(val);
    });
    this.storage.get('sportNames').then((val) => {
      this.sportNames = val;
      for (var key in this.sportNames) {
        if( this.sportNameSetting[this.sportNames[key]] ){
          this.sportNamesFiltered[key] = this.sportNames[key];
        }
      }
    });
    this.storage.get('dailyOdds').then((val) => {
      var i;
      for (i = 0; i < val.length; i++) {
        this.results.push(val[i].Sport);
        this.results = this.results.filter( this.onlyUnique );
      }
    });
  }

  menuClick(nav){
    if(nav == 21){
      this.navCtrl.push(GolfPage, {sport: nav});
    } else if( nav == "settings") {
      this.navCtrl.push(SettingsPage);
    } else {
      this.navCtrl.push(SportsPage, {sport: nav});
    }

  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }


}
