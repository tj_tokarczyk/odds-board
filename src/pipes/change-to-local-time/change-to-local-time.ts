import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';
import 'moment-timezone';

/**
 * Generated class for the ChangeToLocalTimePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'changeToLocalTime',
})
export class ChangeToLocalTimePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string) {
    return moment(value).subtract(4, 'hours').format('ddd h:mm a');
  }
}
